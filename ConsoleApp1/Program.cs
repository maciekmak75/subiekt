﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ConsoleApp1
{
    class Program
    {

       static public void addPozycje(InsERT.SuDokument dokument, string nazwa, int ilosc, string cena)
        {
            var nowaPozycja = dokument.Pozycje.Dodaj(nazwa);
            nowaPozycja.IloscJm = ilosc;
            nowaPozycja.CenaNettoPrzedRabatem = float.Parse(cena, System.Globalization.CultureInfo.InvariantCulture);
            nowaPozycja.Jm = "szt.";
            nowaPozycja.Opis = "test";
        }

        static public string win1250ToUtf(string str)
        {
            Encoding wind1252 = Encoding.GetEncoding(1250);
            Encoding utf8 = Encoding.UTF8;
            byte[] wind1252Bytes = wind1252.GetBytes(str);
            byte[] utf8Bytes = Encoding.Convert(wind1252, utf8, wind1252Bytes);
            string utf8String = Encoding.UTF8.GetString(utf8Bytes);
            return str;
        }
        static void porownaj(string sciezkaNowyXML)
        {
            XmlDocument bazowyXML = new XmlDocument();
            XmlDocument nowyXML = new XmlDocument();
            string sciezkaRefPW = "C:\\Users\\Maciek\\Desktop\\PW.txt";
            string sciezkaRefRW = "C:\\Users\\Maciek\\Desktop\\RW.txt";

            bazowyXML.Load("C:\\Users\\Maciek\\Desktop\\towary3.xml");
            nowyXML.Load(sciezkaNowyXML);

            XmlNodeList listaBazowyXML = bazowyXML.SelectNodes("/Dokument/Line");
            XmlNodeList listaNowyXML = nowyXML.SelectNodes("/Dokument/Line");
            
            bool[] listaWykorzystaneBazowe = new bool[listaBazowyXML.Count];
            bool[] listaWykorzystaneNowe = new bool[listaNowyXML.Count];

            //laczenie z subiektem
            InsERT.GT gt = new InsERT.GT();
            InsERT.Subiekt sgt;
            gt.Produkt = InsERT.ProduktEnum.gtaProduktSubiekt;
            gt.Serwer = "(local)\\InsERTGT";
            gt.Baza = "test";
            gt.Autentykacja = InsERT.AutentykacjaEnum.gtaAutentykacjaMieszana;
            gt.Uzytkownik = "test";
            gt.UzytkownikHaslo = "test";
            gt.Operator = "test";
            gt.OperatorHaslo = "test";
            sgt = (InsERT.Subiekt)gt.Uruchom((int)InsERT.UruchomDopasujEnum.gtaUruchomDopasuj, (int)InsERT.UruchomEnum.gtaUruchom);
            sgt.Okno.Widoczne = true;

            var nowePW = sgt.SuDokumentyManager.DodajPW();
            var noweRW = sgt.SuDokumentyManager.DodajRW();

            int roznicaIlosc = 0;
            try
            {
                for (int i = 0; i < listaBazowyXML.Count; i++)
                {
                    for (int j = 0; j < listaNowyXML.Count; j++)
                    {
                        if (listaBazowyXML[i].SelectSingleNode("ItemCode").InnerText == listaNowyXML[j].SelectSingleNode("ItemCode").InnerText)
                        {
                            roznicaIlosc = Int32.Parse(listaNowyXML[j].SelectSingleNode("Quantity").InnerText) - Int32.Parse(listaBazowyXML[i].SelectSingleNode("Quantity").InnerText);
                            if (roznicaIlosc != 0)
                            {
                                if (roznicaIlosc > 0)
                                    addPozycje(nowePW, listaBazowyXML[i].SelectSingleNode("EAN").InnerText, Math.Abs(roznicaIlosc), listaBazowyXML[i].SelectSingleNode("Price").InnerText);
                                else
                                    addPozycje(noweRW, listaBazowyXML[i].SelectSingleNode("EAN").InnerText, Math.Abs(roznicaIlosc), listaBazowyXML[i].SelectSingleNode("Price").InnerText);
                            }
                            listaWykorzystaneBazowe[i] = true;
                            listaWykorzystaneNowe[j] = true;
                            break; ;
                        }
                    }
                }
                //jesli brak towaru na nowej liscie
                for (int i = 0; i < listaWykorzystaneBazowe.Count(); i++)
                {
                    if (listaWykorzystaneBazowe[i] == false)
                    {
                        addPozycje(noweRW, listaBazowyXML[i].SelectSingleNode("EAN").InnerText,Math.Abs(Int32.Parse(listaBazowyXML[i].SelectSingleNode("Quantity").InnerText)), listaBazowyXML[i].SelectSingleNode("Price").InnerText);
                    }
                }

                //jesli nowy towar na liście
                for (int i = 0; i < listaWykorzystaneNowe.Count(); i++)
                {
                    if (listaWykorzystaneNowe[i] == false)
                    {
                        if (!sgt.Towary.Istnieje(listaBazowyXML[i].SelectSingleNode("EAN").InnerText))
                        {
                            var Tw = sgt.Towary.Dodaj(InsERT.TowarRodzajEnum.gtaTowarRodzajTowar);
                            Tw.Nazwa = win1250ToUtf(listaNowyXML[i].SelectSingleNode("Name").InnerText);
                            Tw.Symbol = listaNowyXML[i].SelectSingleNode("EAN").InnerText;
                            Tw.Opis = win1250ToUtf(listaNowyXML[i].SelectSingleNode("Description").InnerText);
                            Tw.KodyKreskowe.Podstawowy = listaNowyXML[i].SelectSingleNode("EAN").InnerText;
                            Tw.Zapisz();
                        }
                        addPozycje(nowePW, listaNowyXML[i].SelectSingleNode("EAN").InnerText, Int32.Parse(listaNowyXML[i].SelectSingleNode("Quantity").InnerText), listaNowyXML[i].SelectSingleNode("Price").InnerText);
                    }
                }
                nowePW.Zapisz();
                noweRW.Zapisz();
                var starePW = sgt.SuDokumentyManager.Wczytaj(File.ReadAllText(@sciezkaRefPW));
                var stareRW = sgt.SuDokumentyManager.Wczytaj(File.ReadAllText(@sciezkaRefRW));
                if(starePW !=null)
                    starePW.Usun(false);
                if(stareRW !=null)
                    stareRW.Usun(false);
                string idNowePW = nowePW.NumerPelny;
                string idNoweRW = noweRW.NumerPelny;
                File.WriteAllText(@sciezkaRefPW, idNowePW);
                File.WriteAllText(@sciezkaRefRW, idNoweRW);
                nowePW.Zamknij();
                noweRW.Zamknij();
            }
            finally
            {

            }
        }

        static void connectSubiekt()
        {

        }

        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            porownaj("C:\\Users\\Maciek\\Desktop\\towary6.xml");
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();

        }
    }
}
